#!/usr/bin/env bash

docker-compose build --build-arg IP=$(ipconfig getifaddr en0)

docker-compose up -d
docker-compose exec php composer install
